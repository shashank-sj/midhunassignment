# python function to check if function is sorted and in what order
def is_sorted(num_list):
    inc = 1
    dec = -1
    if not num_list:
        return 1
    if len(num_list) == 1:
        return 1
    if num_list[1] >= num_list[0]:
        for i in range(1, len(num_list) - 1):
            if num_list[i + 1] < num_list[i]:
                return 0
        return inc
    else:
       for i in range(1, len(num_list) - 1):
           if num_list[i + 1] > num_list[i]:
               return 0
           return dec

print(is_sorted([]))
print(is_sorted([1, 1, 1, 1]))
print(is_sorted([0, 3, 5, 7, 13]))
print(is_sorted([7, 3, -4, -17]))
print(is_sorted([1, 3, 5, 3, 2]))