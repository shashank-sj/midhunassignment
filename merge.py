
def mergesort(sequence):
    if len(sequence) <= 1:
        return sequence[:]
    else:
        mid = len(sequence)//2
        left = sequence[:mid]
        right = sequence[mid:]
        mergesort(left)
        mergesort(right)
        return merge(left,right)

def merge(left,right):
    i = j = k = 0      
    while i < len(left) and j < len(right): 
        if left[i] < right[j]: 
            sequence[k] = left[i] 
            i+=1
        else: 
            sequence[k] = right[j] 
            j+=1
        k+=1      
    #LeftOverElements    
    while i < len(left): 
        sequence[k] = left[i] 
        i+=1
        k+=1  
    while j < len(right): 
        sequence[k] = right[j] 
        j+=1
        k+=1

def printList(sequence): 
    for i in range(len(sequence)):         
        print(sequence[i],end=" ") 
    print() 

if __name__ == '__main__': 
    sequence = [12, 11, 13, 5, 6, 7]  
    print ("Given array is", end="\n")  
    printList(sequence) 
    mergesort(sequence) 
    print("Sorted array is: ", end="\n") 
    printList(sequence) 